// C# program to find largest of three numbers 
usingSystem;
usingSystem.IO;
usingSystem.Text;
 
namespaceIncludeHelp
{
classTest
 {
// Main Method 
staticvoidMain(string[] args)
 {
inta;
intb;
intc;
intlarge;
 
//input the numbers
Console.Write("Enter first number : ");
a = Convert.ToInt32(Console.ReadLine());
Console.Write("Enter second number: ");
b = Convert.ToInt32(Console.ReadLine());
Console.Write("Enter third number : ");
c = Convert.ToInt32(Console.ReadLine());
 
//finding largest number using if-else
if (a > b && a > c)
large = a;
elseif (b > a && b > c)
large = b;
elselarge = c;
 
//printing 
Console.WriteLine("Using if-else...");
Console.WriteLine("Largest number is {0}", large);
 
//finding largest number using ternary operator 
large = (a > b && a > c) ? a : (b > a && b > c) ? b : c;
 
//printing 
Console.WriteLine("Using ternary operator...");
Console.WriteLine("Largest number is {0}", large);
 
//hit ENTER to exit the program
Console.ReadLine();
 }
 }
}

